/**
 * cute core
 * util: 创建全局变量'_'
 * 创建一些全局方法：方法使用'_'开头
 *  _log
 *
 */

define(function (require) {
    require("./util");
});

(function() {
    this._log = function(msg, type) {
        console && console[type || (type = "log")] && console[type](msg);
    }
}).call(this);