JavaScript Util Functions
noConflict

// Collection Functions
// --------------------
each, forEach
map, collect
find, detect
filter, select
reject // the opposite of filter
every, all
some, any
contains, include
max
min
sortBy
shuffle
size
toArray

// Array Functions
// ---------------
last
first
compact
flatten
without
union
uniq
difference
zip
object
range

// Function Functions
// ------------------
once
after, 阻碍器
compose

// Object Functions
// ----------------
keys
values
pairs
functions, methods
extend
pick
omit
clone
has
is系列 * 15

// Utility Functions
// -----------------
identity
times
random
mixin
uniqueId
escape
unescape

// String Functions
// ----------------
numberFormat
isBlank
capitalize
chop
lines
words
startsWith
endsWith
trim, ltrim, rtrim
truncate
repeat

// Chain Functions
// ---------------
chain
value